#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"
extern void* map_pages(void const*, size_t, int);
#define HEAP_SIZE 4096
extern void debug(const char* fmt, ...);

static struct block_header* get_header(void* contents){
    return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

void test1(){
    debug("Тест 1\n");
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    void* allocator = _malloc(132);
    assert(allocator);
    heap_term();
    debug("Тест 1 пройден!\n");
}
void test2(){
    debug("Тест 2\n");
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    void* allocator1 = _malloc(132);
    assert(allocator1);
    void* allocator2 = _malloc(128);
    assert(allocator2);
    _free(allocator1);
    assert(get_header(allocator1)->is_free);
    assert(get_header(allocator2)->is_free);
    heap_term();
    debug("Тест 2 пройден!\n");
}

void test3(){
    debug("Тест 3\n");
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    void* allocator1 = _malloc(132);
    assert(allocator1);
    void* allocator2 = _malloc(128);
    assert(allocator2);
    void* allocator3 = _malloc(190);
    assert(allocator3);
    _free(allocator2);
    _free(allocator1);
    assert(get_header(allocator1)->is_free);
    assert(get_header(allocator2)->is_free);
    assert(get_header(allocator1)->capacity.bytes==132 +
    size_from_capacity((block_capacity) {.bytes=128}).bytes);
    assert(!get_header(allocator3)->is_free);
    heap_term();
    debug("Тест 3 пройден!\n");
}

void test4(){
    debug("Тест 4\n");
    void* heap = heap_init(15);
    assert(heap);
    void* allocator = _malloc(132);
    assert(allocator);
    heap_term();
    debug("Тест 4 пройден!\n");
}

void test5(){
    debug("Тест 5\n");
    void* pre_allocator = map_pages(HEAP_START, 8, MAP_FIXED);
    assert(pre_allocator);
    void* alloc_in_filled_ptr = _malloc(132);
    assert(alloc_in_filled_ptr);
    assert(pre_allocator != alloc_in_filled_ptr);
    heap_term();
    debug("Тест 5 пройден!\n");
}
int main(){
    test1();
    test2();
    test3();
    test4();
    test5();
    debug("Все тесты пройдены!\n");
}
